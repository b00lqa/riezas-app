package main

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/b00lqa/riezas-app.git/db"
)

func (hnd *ginHandler) deleteItem(ctx *gin.Context) {
	var err error
	jsonBody, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		_ = ctx.AbortWithError(
			http.StatusInternalServerError,
			err,
		)
	}
	var body map[string]string
	err = json.Unmarshal(jsonBody, &body)
	if err != nil {
		_ = ctx.AbortWithError(
			http.StatusInternalServerError,
			err,
		)
	}

	switch table := ctx.Param("table"); table {
	case "accident":
		accident_id, _ := strconv.Atoi(body["AccidentID"])
		err = hnd.queries.DeleteAccident(
			ctx.Request.Context(),
			int32(accident_id),
		)
	case "accident_participants":
		accident_id, _ := strconv.Atoi(body["AccidentID"])
		emp_id, _ := strconv.Atoi(body["EmpID"])
		err = hnd.queries.DeleteAccidentParticipants(
			ctx.Request.Context(),
			db.DeleteAccidentParticipantsParams{
				AccidentID: int32(accident_id),
				EmpID:      int32(emp_id),
			},
		)
	case "chronic":
		chronic_id, _ := strconv.Atoi(body["ChronicID"])
		emp_id, _ := strconv.Atoi(body["EmpID"])
		shift_id, _ := strconv.Atoi(body["ShiftID"])
		err = hnd.queries.DeleteChronic(
			ctx.Request.Context(),
			db.DeleteChronicParams{
				ChornicID: int32(chronic_id),
				EmpID:     int32(emp_id),
				ShiftID:   int32(shift_id),
			},
		)
	case "equipment":
		emp_id, _ := strconv.Atoi(body["EmpID"])
		equipment_id, _ := strconv.Atoi(body["EquipmentID"])
		err = hnd.queries.DeleteEquipment(
			ctx.Request.Context(),
			db.DeleteEquipmentParams{
				EmpID:       int32(emp_id),
				EquipmentID: int32(equipment_id),
			},
		)
	case "report":
		report_id, _ := strconv.Atoi(body["ReportID"])
		emp_id, _ := strconv.Atoi(body["EmpID"])
		accident_id, _ := strconv.Atoi(body["AccidentID"])
		chronic_id, _ := strconv.Atoi(body["ChronicID"])
		err = hnd.queries.DeleteReport(
			ctx.Request.Context(),
			db.DeleteReportParams{
				ReportID:   int32(report_id),
				EmpID:      int32(emp_id),
				AccidentID: int32(accident_id),
				ChronicID:  int32(chronic_id),
			},
		)
	case "shift_participants":
		emp_id, _ := strconv.Atoi(body["EmpID"])
		shift_id, _ := strconv.Atoi(body["ShiftID"])
		err = hnd.queries.DeleteShiftParticipant(
			ctx.Request.Context(),
			db.DeleteShiftParticipantParams{
				EmpID:   int32(emp_id),
				ShiftID: int32(shift_id),
			},
		)
	case "taken_position":
		emp_id, _ := strconv.Atoi(body["EmpID"])
		position_id, _ := strconv.Atoi(body["PositionID"])
		err = hnd.queries.DeleteTakenPosition(
			ctx.Request.Context(),
			db.DeleteTakenPositionParams{
				EmpID:      int32(emp_id),
				PositionID: int32(position_id),
			},
		)
	case "position":
		position_id, _ := strconv.Atoi(body["PositionID"])
		err = hnd.queries.DeletePosition(
			ctx.Request.Context(),
			int32(position_id),
		)
	case "employee":
		emp_id, _ := strconv.Atoi(body["EmpID"])
		err = hnd.queries.DeleteEmployee(
			ctx.Request.Context(),
			int32(emp_id),
		)
	case "shift":
		shift_id, _ := strconv.Atoi(body["ShiftID"])
		err = hnd.queries.DeleteShift(
			ctx.Request.Context(),
			int32(shift_id),
		)
	default:
		ctx.AbortWithStatus(http.StatusNotFound)
	}

	if err != nil {
		_ = ctx.AbortWithError(
			http.StatusInternalServerError,
			err,
		)
	}
}
