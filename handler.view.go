package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/b00lqa/riezas-app.git/db"
)

type ginHandler struct {
	queries *db.Queries
}

func (hnd *ginHandler) showIndex(ctx *gin.Context) {
	tables, err := hnd.queries.GetTablesList(ctx.Request.Context())
	if err != nil {
		_ = ctx.AbortWithError(
			http.StatusInternalServerError,
			err,
		)
	}
	ctx.HTML(
		http.StatusOK,
		"index.html",
		gin.H{
			"Tables": tables,
		},
	)
}

func (hnd *ginHandler) showView(ctx *gin.Context) {
	var err error
	payload := gin.H{}
	template := "view"
	payload["Table"] = ctx.Param("table")
	switch table := ctx.Param("table"); table {
	case "employee":
		template += "Employee.html"
		values, err := hnd.queries.GetAllEmployees(ctx.Request.Context(), []string{})
		if err != nil {
			_ = ctx.AbortWithError(
				http.StatusInternalServerError,
				err,
			)
		}
		payload["tableData"] = values
		payload["Columns"] = values[0].GetColumns()
	case "accident":
		template += "Accident.html"
		values, err := hnd.queries.GetAllAccidents(ctx.Request.Context(), []string{})
		if err != nil {
			_ = ctx.AbortWithError(
				http.StatusInternalServerError,
				err,
			)
		}
		payload["tableData"] = values
		payload["Columns"] = values[0].GetColumns()
	case "accident_participants":
		template += "AccidentParticipant.html"
		values, err := hnd.queries.GetAllAccidentParticipants(ctx.Request.Context(), []string{})
		if err != nil {
			_ = ctx.AbortWithError(
				http.StatusInternalServerError,
				err,
			)
		}
		payload["tableData"] = values
		payload["Columns"] = values[0].GetColumns()
	case "chronic":
		template += "Chronic.html"
		values, err := hnd.queries.GetAllChronics(ctx.Request.Context(), []string{})
		if err != nil {
			_ = ctx.AbortWithError(
				http.StatusInternalServerError,
				err,
			)
		}
		payload["tableData"] = values
		payload["Columns"] = values[0].GetColumns()
	case "equipment":
		template += "Equipment.html"
		values, err := hnd.queries.GetAllEquipment(ctx.Request.Context(), []string{})
		if err != nil {
			_ = ctx.AbortWithError(
				http.StatusInternalServerError,
				err,
			)
		}
		payload["tableData"] = values
		payload["Columns"] = values[0].GetColumns()
	case "position":
		template += "Position.html"
		values, err := hnd.queries.GetAllPositions(ctx.Request.Context(), []string{})
		if err != nil {
			_ = ctx.AbortWithError(
				http.StatusInternalServerError,
				err,
			)
		}
		payload["tableData"] = values
		payload["Columns"] = values[0].GetColumns()
	case "report":
		template += "Report.html"
		values, err := hnd.queries.GetAllReports(ctx.Request.Context(), []string{})
		if err != nil {
			_ = ctx.AbortWithError(
				http.StatusInternalServerError,
				err,
			)
		}
		payload["tableData"] = values
		payload["Columns"] = values[0].GetColumns()
	case "shift":
		template += "Shift.html"
		values, err := hnd.queries.GetAllShifts(ctx.Request.Context(), []string{})
		if err != nil {
			_ = ctx.AbortWithError(
				http.StatusInternalServerError,
				err,
			)
		}
		payload["tableData"] = values
		payload["Columns"] = values[0].GetColumns()
	case "shift_participants":
		template += "ShiftParticipant.html"
		values, err := hnd.queries.GetAllShiftParticipants(ctx.Request.Context(), []string{})
		if err != nil {
			_ = ctx.AbortWithError(
				http.StatusInternalServerError,
				err,
			)
		}
		payload["tableData"] = values
		payload["Columns"] = values[0].GetColumns()
	case "taken_position":
		template += "TakenPosition.html"
		values, err := hnd.queries.GetAllTakenPositions(ctx.Request.Context(), []string{})
		if err != nil {
			_ = ctx.AbortWithError(
				http.StatusInternalServerError,
				err,
			)
		}
		payload["tableData"] = values
		payload["Columns"] = values[0].GetColumns()
	default:
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	if err != nil {
		_ = ctx.AbortWithError(
			http.StatusInternalServerError,
			err,
		)
	}

	ctx.HTML(
		http.StatusOK,
		template,
		payload,
	)
}
