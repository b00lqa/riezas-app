package main

import (
	"encoding/json"
	"io"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (hnd *ginHandler) searchItems(ctx *gin.Context) {
	var err error
	requestBody, err := readJSONBody(ctx.Request.Body)
	if err != nil {
		_ = ctx.AbortWithError(
			http.StatusInternalServerError,
			err,
		)
	}
	var payload = gin.H{}

	switch table := ctx.Param("table"); table {
	case "employee":
		payload["values"], err = hnd.queries.GetAllEmployees(ctx.Request.Context(), requestBody["fields"])
	case "accident":
		payload["values"], err = hnd.queries.GetAllAccidents(ctx.Request.Context(), requestBody["fields"])
	case "accident_participants":
		payload["values"], err = hnd.queries.GetAllAccidentParticipants(ctx.Request.Context(), requestBody["fields"])
	case "chronic":
		payload["values"], err = hnd.queries.GetAllChronics(ctx.Request.Context(), requestBody["fields"])
	case "equipment":
		payload["values"], err = hnd.queries.GetAllEquipment(ctx.Request.Context(), requestBody["fields"])
	case "position":
		payload["values"], err = hnd.queries.GetAllPositions(ctx.Request.Context(), requestBody["fields"])
	case "report":
		payload["values"], err = hnd.queries.GetAllReports(ctx.Request.Context(), requestBody["fields"])
	case "shift":
		payload["values"], err = hnd.queries.GetAllShifts(ctx.Request.Context(), requestBody["fields"])
	case "shift_participants":
		payload["values"], err = hnd.queries.GetAllShiftParticipants(ctx.Request.Context(), requestBody["fields"])
	case "taken_position":
		payload["values"], err = hnd.queries.GetAllTakenPositions(ctx.Request.Context(), requestBody["fields"])
	default:
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	if err != nil {
		_ = ctx.AbortWithError(
			http.StatusInternalServerError,
			err,
		)
	}

	ctx.JSON(http.StatusOK, payload["values"])
}

func readJSONBody(requestBody io.ReadCloser) (map[string][]string, error) {
	jsonBody, err := io.ReadAll(requestBody)
	if err != nil {
		return nil, err
	}
	var body map[string][]string
	err = json.Unmarshal(jsonBody, &body)
	if err != nil {
		return nil, err
	}
	return body, nil
}
