package main

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/b00lqa/riezas-app.git/db"

	_ "github.com/lib/pq"
)

func main() {
	viper.SetConfigFile("./config.yml")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("error reading config file: %v", err)
	}

	conInfo := viper.GetStringMapString("db")
	conn, err := sql.Open(
		"postgres",
		fmt.Sprintf(
			"host=%s dbname=%s user=%s password=%s sslmode=disable",
			conInfo["postgres_host"],
			conInfo["postgres_db"],
			conInfo["postgres_user"],
			conInfo["postgres_password"],
		),
	)
	if err != nil {
		log.Fatalf("error connecting to db: %v", err)
	}
	defer conn.Close()

	queries := db.New(conn)

	router := gin.Default()

	router.LoadHTMLGlob("templates/html/*")

	authorized := router.Group("/", gin.BasicAuth(viper.GetStringMapString("users")))

	hnd := ginHandler{queries: queries}
	authorized.GET("/index", hnd.showIndex)
	authorized.GET("/view/:table", hnd.showView)

	authorized.POST("/view/:table", hnd.searchItems)
	authorized.POST("/save/:table", hnd.saveItem)
	authorized.POST("/delete/:table", hnd.deleteItem)

	_ = router.Run(":80")
}
