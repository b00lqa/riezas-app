package db

import "context"

func (q *Queries) GetAllEquipment(ctx context.Context, filter []string) ([]Equipment, error) {
	query, args, err := buildSelectQuery("equipment", filter)
	if err != nil {
		return nil, err
	}

	rows, err := q.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var items []Equipment
	for rows.Next() {
		var i Equipment
		if err := rows.Scan(
			&i.EmpID,
			&i.EquipmentID,
			&i.Name,
			&i.Status,
			&i.Price,
			&i.PurchaseDate,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
