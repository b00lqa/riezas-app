-- name: DeleteAccident :exec
DELETE FROM accident WHERE accident_id = @accident_id::integer;

-- name: DeleteAccidentParticipants :exec
DELETE FROM accident_participants WHERE accident_id = @accident_id::integer 
AND emp_id = @emp_id::integer;

-- name: DeleteChronic :exec
DELETE FROM chronic WHERE chornic_id = @chornic_id::integer 
AND emp_id = @emp_id::integer
AND shift_id = @shift_id::integer;

-- name: DeleteEmployee :exec 
DELETE FROM employee WHERE emp_id = @emp_id::integer;

-- name: DeleteEquipment :exec
DELETE FROM equipment WHERE emp_id = @emp_id::integer
AND equipment_id = @equipment_id::integer;

-- name: DeletePosition :exec
DELETE FROM position WHERE position_id = @position_id::integer;

-- name: DeleteReport :exec
DELETE FROM report WHERE report_id = @report_id::integer 
AND emp_id = @emp_id::integer
AND accident_id = @accident_id::integer 
AND chornic_id = @chronic_id::integer;

-- name: DeleteShift :exec 
DELETE FROM shift WHERE shift_id = @shift_id::integer;

-- name: DeleteShiftParticipant :exec
DELETE FROM shift_participants WHERE emp_id = @emp_id::integer 
AND shift_id = @shift_id::integer;

-- name: DeleteTakenPosition :exec
DELETE FROM taken_position WHERE emp_id = @emp_id::integer 
AND position_id = @position_id;
