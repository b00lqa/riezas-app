-- name: SaveAccident :exec
INSERT INTO accident (start_time, end_time, "type")
VALUES(@start_time::timestamp, @end_time::timestamp, @type::text);

-- name: SaveAccidentParticipants :exec 
INSERT INTO accident_participants (emp_id, accident_id, arrival)
VALUES(@emp_id::integer, @accident_id::integer, @arrival::timestamp);

-- name: SaveChronic :exec
INSERT INTO chronic (emp_id, shift_id, file_link, upload_date)
VALUES(@emp_id::integer, @shift_id::integer, @file_link::text, @upload_date::timestamp);

-- name: SaveEmployee :exec
INSERT INTO employee (full_name, birth_date, pasport_series, 
                      pasport_number, itn, inila, phone_number, 
                      email, account_number, bank_details, 
                      registration_address, residential_address, personal_life, 
                      sex)
VALUES(@full_name::text, @birth_date::timestamp, @pasport_series::text,
       @pasport_number::text, @itn::text, @inila::text,
       @phone_number::text, @email::text, @account_number::text,
       @bank_details::text, @registration_address::text, @residential_address::text,
       @personal_life::text, @sex::text);

-- name: SaveEquipment :exec
INSERT INTO equipment (emp_id, name, status, price, purchase_date)
VALUES(@emp_id::integer, @name::text, @status::text, @price::integer, @purchase_date::timestamp);

-- name: SavePosition :exec
INSERT INTO position (name, salary, schedule)
VALUES(@name::text, @salary::integer, @schedule::text);

-- name: SaveReport :exec
INSERT INTO report (emp_id, accident_id, chronic_id, file_link, upload_date)
VALUES(@emp_id::integer, @accident_id::integer, @chronic_id::integer, @file_link::text, @upload_date::timestamp);

-- name: SaveShift :exec
INSERT INTO shift (address, start_time, end_time)
VALUES(@address::text, @start_time::timestamp, @end_time::timestamp);

-- name: SaveShiftParticipants :exec
INSERT INTO shift_participants (emp_id, shift_id)
VALUES(@emp_id::integer, @shift_id::integer);

-- name: SaveTakenPosition :exec
INSERT INTO taken_position (emp_id, position_id, entry_date)
VALUES(@emp_id::integer, @position_id::integer, @entry_date::timestamp);