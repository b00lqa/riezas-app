package db

import "context"

func (q *Queries) GetAllPositions(ctx context.Context, filter []string) ([]Position, error) {
	query, args, err := buildSelectQuery("position", filter)
	if err != nil {
		return nil, err
	}

	rows, err := q.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var items []Position
	for rows.Next() {
		var i Position
		if err := rows.Scan(
			&i.PositionID,
			&i.Name,
			&i.Salary,
			&i.Schedule,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
