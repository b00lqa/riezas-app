package db

import "context"

func (q *Queries) GetAllEmployees(ctx context.Context, filter []string) ([]Employee, error) {
	query, args, err := buildSelectQuery("employee", filter)
	if err != nil {
		return nil, err
	}

	rows, err := q.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var items []Employee
	for rows.Next() {
		var i Employee
		if err := rows.Scan(
			&i.EmpID,
			&i.FullName,
			&i.BirthDate,
			&i.PasportSeries,
			&i.PasportNumber,
			&i.Itn,
			&i.Inila,
			&i.PhoneNumber,
			&i.Email,
			&i.AccountNumber,
			&i.BankDetails,
			&i.RegistrationAddress,
			&i.ResidentialAddress,
			&i.PersonalFile,
			&i.Sex,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
