-- +goose Up
-- +goose StatementBegin
CREATE TABLE Shift_participants
( 
	Emp_ID               SERIAL REFERENCES Employee(Emp_ID),
	Shift_ID             SERIAL REFERENCES Shift(Shift_ID),
	PRIMARY KEY (Emp_ID, Shift_ID)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE Shift_participants;
-- +goose StatementEnd
