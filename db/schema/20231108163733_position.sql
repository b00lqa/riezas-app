-- +goose Up
-- +goose StatementBegin
CREATE TABLE Position
( 
	Position_ID          SERIAL PRIMARY KEY,
	Name                 VARCHAR(50) ,
	Salary               MONEY ,
	Schedule             VARCHAR(255) 
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE Position;
-- +goose StatementEnd
