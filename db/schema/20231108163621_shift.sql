-- +goose Up
-- +goose StatementBegin
CREATE TABLE Shift
( 
	Shift_ID             SERIAL PRIMARY KEY,
	Address              VARCHAR(255) ,
	Start_time           timestamp ,
	End_time             timestamp 
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE Shift;
-- +goose StatementEnd
