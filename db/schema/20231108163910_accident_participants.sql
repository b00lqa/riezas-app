-- +goose Up
-- +goose StatementBegin
CREATE TABLE Accident_participants
( 
	Emp_ID               SERIAL REFERENCES Employee(Emp_ID),
	Accident_ID          SERIAL REFERENCES Accident(Accident_ID),
	Arrival              timestamp,
    PRIMARY KEY (Emp_ID, Accident_ID)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE Accident_participants;
-- +goose StatementEnd
