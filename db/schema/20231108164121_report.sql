-- +goose Up
-- +goose StatementBegin
CREATE TABLE Report
( 
	Report_ID            SERIAL PRIMARY KEY,
    Emp_ID               SERIAL REFERENCES Employee(Emp_ID),
	Accident_ID          SERIAL REFERENCES Accident(Accident_ID),
	Chronic_ID           SERIAL  REFERENCES Chronic(Chronic_ID),
	File_link            VARCHAR(255) ,
	Upload_date          timestamp 
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE Report;
-- +goose StatementEnd
