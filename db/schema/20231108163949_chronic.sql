-- +goose Up
-- +goose StatementBegin
CREATE TABLE Chronic
( 
	Chronic_ID           SERIAL PRIMARY KEY,
	Emp_ID               SERIAL REFERENCES Employee(Emp_ID),
	Shift_ID             SERIAL REFERENCES Shift(Shift_ID),
	File_link            VARCHAR(255) ,
	Upload_date          timestamp
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE Chronic;
-- +goose StatementEnd
