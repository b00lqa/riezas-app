-- +goose Up
-- +goose StatementBegin
CREATE TABLE Equipment
( 
	Emp_ID               SERIAL REFERENCES Employee(Emp_ID),
	Equipment_ID         SERIAL PRIMARY KEY,
	Name                 VARCHAR(50) ,
	Status               VARCHAR(50) ,
	Price                MONEY ,
	Purchase_date        timestamp 
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE Equipment;
-- +goose StatementEnd
