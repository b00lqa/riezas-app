-- +goose Up
-- +goose StatementBegin
CREATE TABLE Employee
( 
	Emp_ID               SERIAL PRIMARY KEY,
	Full_name            VARCHAR(100) ,
	Birth_date           timestamp ,
	Pasport_series       VARCHAR(10) ,
	Pasport_number       VARCHAR(10) ,
	ITN                  VARCHAR(50) ,
	INILA                VARCHAR(50) ,
	Phone_number         VARCHAR(50) ,
	Email                VARCHAR(50) ,
	Account_number       VARCHAR(50) ,
	Bank_details         VARCHAR(255) ,
	Registration_address VARCHAR(255) ,
	Residential_address  VARCHAR(255) ,
	Personal_file        VARCHAR(255) ,
	Sex                  VARCHAR(10) 
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE Employee
-- +goose StatementEnd
