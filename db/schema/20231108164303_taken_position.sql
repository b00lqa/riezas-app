-- +goose Up
-- +goose StatementBegin
CREATE TABLE Taken_position
( 
	Emp_ID               SERIAL REFERENCES Employee(Emp_ID),
	Position_ID          SERIAL REFERENCES Position(Position_ID),
	Entry_date           timestamp,
    PRIMARY KEY (Emp_ID, Position_ID)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE Taken_position;
-- +goose StatementEnd
