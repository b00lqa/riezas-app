-- +goose Up
-- +goose StatementBegin
CREATE TABLE Accident
( 
	Accident_ID          SERIAL PRIMARY KEY,
	Start_time           timestamp ,
	End_time             timestamp ,
	Type                 VARCHAR(50) 
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE Accident;
-- +goose StatementEnd
