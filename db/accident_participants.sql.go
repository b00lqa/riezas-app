package db

import "context"

func (q *Queries) GetAllAccidentParticipants(ctx context.Context, filter []string) ([]AccidentParticipant, error) {
	query, args, err := buildSelectQuery("accident_participants", filter)
	if err != nil {
		return nil, err
	}

	rows, err := q.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var items []AccidentParticipant
	for rows.Next() {
		var i AccidentParticipant
		if err := rows.Scan(
			&i.EmpID,
			&i.AccidentID,
			&i.Arrival,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
