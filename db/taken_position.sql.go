package db

import "context"

func (q *Queries) GetAllTakenPositions(ctx context.Context, filter []string) ([]TakenPosition, error) {
	query, args, err := buildSelectQuery("taken_position", filter)
	if err != nil {
		return nil, err
	}

	rows, err := q.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var items []TakenPosition
	for rows.Next() {
		var i TakenPosition
		if err := rows.Scan(
			&i.EmpID,
			&i.PositionID,
			&i.EntryDate,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
