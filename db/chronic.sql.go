package db

import "context"

func (q *Queries) GetAllChronics(ctx context.Context, filter []string) ([]Chronic, error) {
	query, args, err := buildSelectQuery("chronic", filter)
	if err != nil {
		return nil, err
	}

	rows, err := q.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var items []Chronic
	for rows.Next() {
		var i Chronic
		if err := rows.Scan(
			&i.ChronicID,
			&i.EmpID,
			&i.ShiftID,
			&i.FileLink,
			&i.UploadDate,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
