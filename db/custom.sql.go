package db

import "reflect"

func (e Employee) GetColumns() []string {
	typeOf := reflect.TypeOf(e)
	columns := make([]string, typeOf.NumField())
	for i := 0; i < typeOf.NumField(); i++ {
		columns[i] = typeOf.Field(i).Name
	}
	return columns
}

func (e Accident) GetColumns() []string {
	typeOf := reflect.TypeOf(e)
	columns := make([]string, typeOf.NumField())
	for i := 0; i < typeOf.NumField(); i++ {
		columns[i] = typeOf.Field(i).Name
	}
	return columns
}

func (e Chronic) GetColumns() []string {
	typeOf := reflect.TypeOf(e)
	columns := make([]string, typeOf.NumField())
	for i := 0; i < typeOf.NumField(); i++ {
		columns[i] = typeOf.Field(i).Name
	}
	return columns
}

func (e Equipment) GetColumns() []string {
	typeOf := reflect.TypeOf(e)
	columns := make([]string, typeOf.NumField())
	for i := 0; i < typeOf.NumField(); i++ {
		columns[i] = typeOf.Field(i).Name
	}
	return columns
}

func (e Position) GetColumns() []string {
	typeOf := reflect.TypeOf(e)
	columns := make([]string, typeOf.NumField())
	for i := 0; i < typeOf.NumField(); i++ {
		columns[i] = typeOf.Field(i).Name
	}
	return columns
}

func (e Report) GetColumns() []string {
	typeOf := reflect.TypeOf(e)
	columns := make([]string, typeOf.NumField())
	for i := 0; i < typeOf.NumField(); i++ {
		columns[i] = typeOf.Field(i).Name
	}
	return columns
}

func (e Shift) GetColumns() []string {
	typeOf := reflect.TypeOf(e)
	columns := make([]string, typeOf.NumField())
	for i := 0; i < typeOf.NumField(); i++ {
		columns[i] = typeOf.Field(i).Name
	}
	return columns
}

func (e ShiftParticipant) GetColumns() []string {
	typeOf := reflect.TypeOf(e)
	columns := make([]string, typeOf.NumField())
	for i := 0; i < typeOf.NumField(); i++ {
		columns[i] = typeOf.Field(i).Name
	}
	return columns
}

func (e TakenPosition) GetColumns() []string {
	typeOf := reflect.TypeOf(e)
	columns := make([]string, typeOf.NumField())
	for i := 0; i < typeOf.NumField(); i++ {
		columns[i] = typeOf.Field(i).Name
	}
	return columns
}

func (e AccidentParticipant) GetColumns() []string {
	typeOf := reflect.TypeOf(e)
	columns := make([]string, typeOf.NumField())
	for i := 0; i < typeOf.NumField(); i++ {
		columns[i] = typeOf.Field(i).Name
	}
	return columns
}
