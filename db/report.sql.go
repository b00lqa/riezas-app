package db

import "context"

func (q *Queries) GetAllReports(ctx context.Context, filter []string) ([]Report, error) {
	query, args, err := buildSelectQuery("report", filter)
	if err != nil {
		return nil, err
	}

	rows, err := q.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var items []Report
	for rows.Next() {
		var i Report
		if err := rows.Scan(
			&i.ReportID,
			&i.EmpID,
			&i.AccidentID,
			&i.ChronicID,
			&i.FileLink,
			&i.UploadDate,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
