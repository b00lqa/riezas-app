package db

import (
	"strings"

	sq "github.com/Masterminds/squirrel"
)

var mapping = map[string]string{
	// IDs
	"EmpID":       "emp_id",
	"AccidentID":  "accident_id",
	"EquipmentID": "equipment_id",
	"PositionID":  "position_id",
	"ReportID":    "report_id",
	"ChronicID":   "chronic_id",
	"ShiftID":     "shift_id",

	// Fields.
	"FullName":            "full_name",
	"BirthDate":           "birth_date",
	"PasportSeries":       "pasport_series",
	"PasportNumber":       "pasport_number",
	"PhoneNumber":         "phone_number",
	"AccountNumber":       "account_number",
	"BankDetails":         "bank_details",
	"RegistrationAddress": "registration_address",
	"ResidentialAddress":  "residential_address",
	"PersonalFile":        "personal_file",
	"StartTime":           "start_time",
	"EndTime":             "end_time",
	"PurchaseDate":        "purchase_date",
	"FileLink":            "file_link",
	"UploadDate":          "upload_date",
	"EntryDate":           "entry_date",
}

func buildSelectQuery(tableName string, filter []string) (string, []interface{}, error) {
	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	sqQuery := psql.Select("*").From(tableName)
	for i := 0; i < len(filter); i += 3 {
		operation, value := filter[i+1], filter[i+2]

		column, ok := mapping[filter[i]]
		if !ok {
			column = strings.ToLower(filter[i])
		}

		switch operation {
		case "=":
			sqQuery = sqQuery.Where(sq.Eq{column: value})
		case ">":
			sqQuery = sqQuery.Where(sq.Gt{column: value})
		case "<":
			sqQuery = sqQuery.Where(sq.Lt{column: value})
		case "LIKE":
			sqQuery = sqQuery.Where(sq.Like{column: "%" + value + "%"})
		}
	}

	return sqQuery.ToSql()
}
