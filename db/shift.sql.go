package db

import "context"

func (q *Queries) GetAllShifts(ctx context.Context, filter []string) ([]Shift, error) {
	query, args, err := buildSelectQuery("shift", filter)
	if err != nil {
		return nil, err
	}

	rows, err := q.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var items []Shift
	for rows.Next() {
		var i Shift
		if err := rows.Scan(
			&i.ShiftID,
			&i.Address,
			&i.StartTime,
			&i.EndTime,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
