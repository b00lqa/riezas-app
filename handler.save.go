package main

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/b00lqa/riezas-app.git/db"
)

func (hnd *ginHandler) saveItem(ctx *gin.Context) {
	var err error
	jsonBody, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		_ = ctx.AbortWithError(
			http.StatusInternalServerError,
			err,
		)
	}
	var body map[string]string
	err = json.Unmarshal(jsonBody, &body)
	if err != nil {
		_ = ctx.AbortWithError(
			http.StatusInternalServerError,
			err,
		)
	}

	// hnd.queries.SaveItem(table, body)
	bodyTimeLayout := "2006-01-02"

	switch table := ctx.Param("table"); table {
	case "accident":
		start_time, _ := time.Parse(bodyTimeLayout, body["StartTime"])
		end_time, _ := time.Parse(bodyTimeLayout, body["EndTime"])
		err = hnd.queries.SaveAccident(
			ctx.Request.Context(),
			db.SaveAccidentParams{
				StartTime: start_time,
				EndTime:   end_time,
				Type:      body["Type"],
			},
		)
	case "accident_participants":
		emp_id, _ := strconv.Atoi(body["EmpID"])
		accident_id, _ := strconv.Atoi(body["AccidentID"])
		arrival, _ := time.Parse(bodyTimeLayout, body["Arrival"])
		err = hnd.queries.SaveAccidentParticipants(
			ctx.Request.Context(),
			db.SaveAccidentParticipantsParams{
				EmpID:      int32(emp_id),
				AccidentID: int32(accident_id),
				Arrival:    arrival,
			},
		)
	case "chronic":
		emp_id, _ := strconv.Atoi(body["EmpID"])
		shift_id, _ := strconv.Atoi(body["ShiftID"])
		upload_date, _ := time.Parse(bodyTimeLayout, body["UploadDate"])
		err = hnd.queries.SaveChronic(
			ctx.Request.Context(),
			db.SaveChronicParams{
				EmpID:      int32(emp_id),
				ShiftID:    int32(shift_id),
				FileLink:   body["FileLink"],
				UploadDate: upload_date,
			},
		)
	case "equipment":
		emp_id, _ := strconv.Atoi(body["EmpID"])
		price, _ := strconv.Atoi(body["Price"])
		purchase_date, _ := time.Parse(bodyTimeLayout, body["PurchaseDate"])
		err = hnd.queries.SaveEquipment(
			ctx.Request.Context(),
			db.SaveEquipmentParams{
				EmpID:        int32(emp_id),
				Name:         body["Name"],
				Status:       body["Status"],
				Price:        int32(price),
				PurchaseDate: purchase_date,
			},
		)
	case "report":
		emp_id, _ := strconv.Atoi(body["EmpID"])
		accident_id, _ := strconv.Atoi(body["AccidentID"])
		chronic_id, _ := strconv.Atoi(body["ChronicID"])
		upload_date, _ := time.Parse(bodyTimeLayout, body["UploadDate"])
		err = hnd.queries.SaveReport(
			ctx.Request.Context(),
			db.SaveReportParams{
				EmpID:      int32(emp_id),
				AccidentID: int32(accident_id),
				ChronicID:  int32(chronic_id),
				FileLink:   body["FileLink"],
				UploadDate: upload_date,
			},
		)
	case "shift_participants":
		emp_id, _ := strconv.Atoi(body["EmpID"])
		shift_id, _ := strconv.Atoi(body["ShiftID"])
		err = hnd.queries.SaveShiftParticipants(
			ctx.Request.Context(),
			db.SaveShiftParticipantsParams{
				EmpID:   int32(emp_id),
				ShiftID: int32(shift_id),
			},
		)
	case "taken_position":
		emp_id, _ := strconv.Atoi(body["EmpID"])
		position_id, _ := strconv.Atoi(body["PositionID"])
		entry_date, _ := time.Parse(bodyTimeLayout, body["EntryDate"])
		err = hnd.queries.SaveTakenPosition(
			ctx.Request.Context(),
			db.SaveTakenPositionParams{
				EmpID:      int32(emp_id),
				PositionID: int32(position_id),
				EntryDate:  entry_date,
			},
		)
	case "position":
		salary, _ := strconv.Atoi("Salary")
		err = hnd.queries.SavePosition(
			ctx.Request.Context(),
			db.SavePositionParams{
				Name:     body["Name"],
				Salary:   int32(salary),
				Schedule: body["Schedule"],
			},
		)
	case "employee":
		birth_date, _ := time.Parse(bodyTimeLayout, body["BirthDate"])
		err = hnd.queries.SaveEmployee(
			ctx.Request.Context(),
			db.SaveEmployeeParams{
				FullName:            body["FullName"],
				BirthDate:           birth_date,
				PasportSeries:       body["PasportSeries"],
				PasportNumber:       body["PasportNumber"],
				Itn:                 body["Itn"],
				Inila:               body["Inila"],
				PhoneNumber:         body["PhoneNumber"],
				Email:               body["Email"],
				AccountNumber:       body["AccountNumber"],
				BankDetails:         body["BankDetails"],
				RegistrationAddress: body["RegistrationAddress"],
				ResidentialAddress:  body["ResidentialAddress"],
				PersonalLife:        body["PersonalLife"],
				Sex:                 body["Sex"],
			},
		)
	case "shift":
		start_time, _ := time.Parse(bodyTimeLayout, body["StartTime"])
		end_time, _ := time.Parse(bodyTimeLayout, body["EndTime"])
		err = hnd.queries.SaveShift(
			ctx.Request.Context(),
			db.SaveShiftParams{
				Address:   body["Address"],
				StartTime: start_time,
				EndTime:   end_time,
			},
		)
	default:
		ctx.AbortWithStatus(http.StatusNotFound)
	}

	if err != nil {
		_ = ctx.AbortWithError(
			http.StatusInternalServerError,
			err,
		)
	}

}
